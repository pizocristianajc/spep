package uniajc.edu.spep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uniajc.edu.spep.exception.ResourceNotFoundException;
import uniajc.edu.spep.model.ProyectoModel;
import uniajc.edu.spep.repository.ProyectoRepository;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class ProyectoController {

    @Autowired
    ProyectoRepository proyectoRepository;

    @GetMapping("/proyecto")
    public List<ProyectoModel> getAllProyecto() { return proyectoRepository.findAll();}

    @PostMapping("/proyecto")
    public ProyectoModel createProyecto(@Valid @RequestBody ProyectoModel proyectoModel) {
        return proyectoRepository.save(proyectoModel);
    }

    @GetMapping("/proyecto/{id_proyecto}")
    public ProyectoModel getProyectoById(@PathVariable(value = "id_proyecto") Long id_proyecto) {
        return proyectoRepository.findById(id_proyecto)
                .orElseThrow(() -> new ResourceNotFoundException("Proyecto", "id_proyecto", id_proyecto));
    }

    @PutMapping("/proyecto/{id_proyecto}")
    public ProyectoModel updateProyecto(@PathVariable(value = "id_proyecto") Long id_proyecto,
                                                  @Valid @RequestBody ProyectoModel proyectoDetails) {

        ProyectoModel proyectoModel = proyectoRepository.findById(id_proyecto)
                .orElseThrow(() -> new ResourceNotFoundException("Proyecto", "id_proyecto", id_proyecto));

        proyectoModel.setDuracion(proyectoDetails.getDuracion());
        proyectoModel.setId_equipo_trabajo(proyectoDetails.getId_equipo_trabajo());
        proyectoModel.setHistoria_max(proyectoDetails.getHistoria_max());
        proyectoModel.setHistoria_min(proyectoDetails.getHistoria_min());
        proyectoModel.setNombre(proyectoDetails.getNombre());
        proyectoModel.setPeso(proyectoDetails.getPeso());
        proyectoModel.setPivote(proyectoDetails.getPivote());
        proyectoModel.setTiempo(proyectoDetails.getTiempo());

        ProyectoModel updatedProyecto = proyectoRepository.save(proyectoModel);
        return updatedProyecto;
    }

    @DeleteMapping("/proyecto/{id_proyecto}")
    public ResponseEntity<?> deleteProyecto(@PathVariable(value = "id_proyecto") Long id_proyecto) {
        ProyectoModel updatedProyecto = proyectoRepository.findById(id_proyecto)
                .orElseThrow(() -> new ResourceNotFoundException("Proyecto", "id_proyecto", id_proyecto));

        proyectoRepository.delete(updatedProyecto);

        return ResponseEntity.ok().build();
    }
}
