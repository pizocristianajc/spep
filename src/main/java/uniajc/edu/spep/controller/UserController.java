package uniajc.edu.spep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uniajc.edu.spep.exception.ResourceNotFoundException;
import uniajc.edu.spep.model.UserModel;
import uniajc.edu.spep.repository.UserRepository;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/user")
    public List<UserModel> getAllUser() { return userRepository.findAll();}

    @PostMapping("/user")
    public UserModel createUser(@Valid @RequestBody UserModel userModel) {
        return userRepository.save(userModel);
    }

    @GetMapping("/user/{id_user}")
    public UserModel getUserById(@PathVariable(value = "id_user") Long id_user) {
        return userRepository.findById(id_user)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id_user", id_user));
    }

    @PutMapping("/user/{id_user}")
    public UserModel updateUser(@PathVariable(value = "id_user") Long id_user,
                              @Valid @RequestBody UserModel userDetails) {

        UserModel userModel = userRepository.findById(id_user)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id_user", id_user));

        userModel.setRol(userDetails.getRol());
        userModel.setCorreo(userDetails.getCorreo());
        userModel.setPass(userDetails.getPass());

        UserModel updatedUser = userRepository.save(userModel);
        return updatedUser;
    }

    @DeleteMapping("/user/{id_user}")
    public ResponseEntity<?> deleteRol(@PathVariable(value = "id_user") Long id_user) {
        UserModel updatedUser = userRepository.findById(id_user)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id_user", id_user));

        userRepository.delete(updatedUser);

        return ResponseEntity.ok().build();
    }
}
