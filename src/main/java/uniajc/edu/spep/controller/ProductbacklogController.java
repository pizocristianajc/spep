package uniajc.edu.spep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uniajc.edu.spep.exception.ResourceNotFoundException;
import uniajc.edu.spep.model.ProductbacklogModel;
import uniajc.edu.spep.repository.ProducbacklogRepository;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class ProductbacklogController {

    @Autowired
    ProducbacklogRepository producbacklogRepository;

    @GetMapping("/producbacklog")
    public List<ProductbacklogModel> getAllProducbacklog() { return producbacklogRepository.findAll();}

    @PostMapping("/producbacklog")
    public ProductbacklogModel createProducbacklog(@Valid @RequestBody ProductbacklogModel productbacklogModel) {
        return producbacklogRepository.save(productbacklogModel);
    }

    @GetMapping("/producbacklog/{id_productbacklog}")
    public ProductbacklogModel getProducbacklogById(@PathVariable(value = "id_productbacklog") Long id_productbacklog) {
        return producbacklogRepository.findById(id_productbacklog)
                .orElseThrow(() -> new ResourceNotFoundException("Producbacklog", "id_productbacklog", id_productbacklog));
    }

    @PutMapping("/producbacklog/{id_productbacklog}")
    public ProductbacklogModel updateProducbacklog(@PathVariable(value = "id_productbacklog") Long id_productbacklog,
                                                   @Valid @RequestBody ProductbacklogModel producbacklogDetails) {

        ProductbacklogModel productbacklogModel = producbacklogRepository.findById(id_productbacklog)
                .orElseThrow(() -> new ResourceNotFoundException("Producbacklog", "id_productbacklog", id_productbacklog));

        productbacklogModel.setComplejidad(producbacklogDetails.getComplejidad());
        productbacklogModel.setPrioridad(producbacklogDetails.getPrioridad());
        productbacklogModel.setId_historia_usuario(producbacklogDetails.getId_historia_usuario());

        ProductbacklogModel updatedProducbacklog = producbacklogRepository.save(productbacklogModel);
        return updatedProducbacklog;
    }

    @DeleteMapping("/producbacklog/{id_productbacklog}")
    public ResponseEntity<?> deleteProducbacklog(@PathVariable(value = "id_productbacklog") Long id_productbacklog) {
        ProductbacklogModel updatedProducbacklog = producbacklogRepository.findById(id_productbacklog)
                .orElseThrow(() -> new ResourceNotFoundException("Producbacklog", "id_productbacklog", id_productbacklog));

        producbacklogRepository.delete(updatedProducbacklog);

        return ResponseEntity.ok().build();
    }
}
