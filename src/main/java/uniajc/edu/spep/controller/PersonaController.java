package uniajc.edu.spep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uniajc.edu.spep.exception.ResourceNotFoundException;
import uniajc.edu.spep.model.PersonaModel;
import uniajc.edu.spep.repository.PersonaRepository;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class PersonaController {

    @Autowired
    PersonaRepository personaRepository;

    @GetMapping("/persona")
    public List<PersonaModel> getAllPersona() {
        return personaRepository.findAll();
    }

    @PostMapping("/persona")
    public PersonaModel createPersona(@Valid @RequestBody PersonaModel persona) {
        return personaRepository.save(persona);
    }

    @GetMapping("/persona/{id_persona}")
    public PersonaModel getId(@PathVariable(value = "id_persona") Long Id) {
        return personaRepository.findById(Id)
                .orElseThrow(() -> new ResourceNotFoundException("Persona", "id_persona", Id));
    }

    @PutMapping("/persona/{id_persona}")
    public PersonaModel updatePersona(@PathVariable(value = "id_persona") Long Id,
                                           @Valid @RequestBody PersonaModel personaDetails) {

        PersonaModel persona = personaRepository.findById(Id)
                .orElseThrow(() -> new ResourceNotFoundException("Persona", "id_persona", Id));

        persona.setNombre(personaDetails.getNombre());
        persona.setApellido(personaDetails.getApellido());
        persona.setDireccion(personaDetails.getDireccion());
        persona.setTelefono(personaDetails.getTelefono());


        PersonaModel updatedPersona = personaRepository.save(persona);
        return updatedPersona;
    }

    @DeleteMapping("/persona/{id_persona}")
    public ResponseEntity<?> deletePersona(@PathVariable(value = "id_persona") Long Id) {
        PersonaModel persona = personaRepository.findById(Id)
                .orElseThrow(() -> new ResourceNotFoundException("Persona", "id_persona", Id));

        personaRepository.delete(persona);

        return ResponseEntity.ok().build();
    }
}
