package uniajc.edu.spep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uniajc.edu.spep.model.Equipo_trabajoModel;


@Repository
public interface EquipoTrabajoRepository extends JpaRepository<Equipo_trabajoModel, Long> {

}
