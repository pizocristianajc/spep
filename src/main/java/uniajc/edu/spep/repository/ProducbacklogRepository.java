package uniajc.edu.spep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uniajc.edu.spep.model.ProductbacklogModel;


@Repository
public interface ProducbacklogRepository extends JpaRepository<ProductbacklogModel, Long> {

}
