package uniajc.edu.spep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uniajc.edu.spep.model.PersonaModel;


@Repository
public interface PersonaRepository extends JpaRepository<PersonaModel, Long> {

}
