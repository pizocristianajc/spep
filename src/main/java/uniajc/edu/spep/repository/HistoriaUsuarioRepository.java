package uniajc.edu.spep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uniajc.edu.spep.model.Historia_usuarioModel;


@Repository
public interface HistoriaUsuarioRepository extends JpaRepository<Historia_usuarioModel, Long> {

}
