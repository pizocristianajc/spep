package uniajc.edu.spep.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@Entity
@Table(name = "tb_historia_usuario")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Historia_usuarioModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long historia_usuario_id;

    @NotBlank
    private String criterio_aceptacion;

    @NotBlank
    private String nombre;

    @NotBlank
    private String funsionalidad;

    @NotBlank
    private String razon;

    @NotBlank
    private String rol;

    @NotBlank
    private Long id_persona;

    public Long getHistoria_usuario_id() {
        return historia_usuario_id;
    }

    public void setHistoria_usuario_id(Long historia_usuario_id) {
        this.historia_usuario_id = historia_usuario_id;
    }

    public String getCriterio_aceptacion() {
        return criterio_aceptacion;
    }

    public void setCriterio_aceptacion(String criterio_aceptacion) {
        this.criterio_aceptacion = criterio_aceptacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFunsionalidad() {
        return funsionalidad;
    }

    public void setFunsionalidad(String funsionalidad) {
        this.funsionalidad = funsionalidad;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Long getId_persona() {
        return id_persona;
    }

    public void setId_persona(Long id_persona) {
        this.id_persona = id_persona;
    }
}
